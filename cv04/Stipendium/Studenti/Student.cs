﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    public abstract class Student
    {
        protected byte delkaStudia;
        private string name;
        private int id;
        private static int lastId;

        static Student()
        {
            lastId = 0;
        }

        private static int getNewid()
        {
            lastId++;
            return lastId;
        }

        public Student(string name)
        {
            this.id = Student.getNewid();
            this.name = name;
        }

        public abstract double CountScholarship();

        public virtual void GetCaption()
        {
            Console.WriteLine("---------Student");
            Console.WriteLine(name);
            Console.WriteLine(id);
            Console.WriteLine(delkaStudia);
        }
    }
}
