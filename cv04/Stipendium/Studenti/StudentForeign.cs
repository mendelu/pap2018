﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    class StudentForeign :Student
    {
        private bool smlouvaVymena;

        public StudentForeign(string ciziJmeno)
            : base(ciziJmeno)
        {
            smlouvaVymena = false;
        }

        public StudentForeign(string ciziJmeno, bool smlouvaVymena)
            : this(ciziJmeno)
        {
            this.smlouvaVymena = smlouvaVymena;
        }

        public override double CountScholarship()
        {
            return (smlouvaVymena) ? 4000 : 8000;
        }

        public override void GetCaption()
        {
            base.GetCaption();
            Console.WriteLine("Sk");
            Console.WriteLine(smlouvaVymena);
        }
    }
}
