﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> mujSeznam = new List<Student>();
            mujSeznam.Add( new StudentCz("Alenka Cz", 500) );
            mujSeznam.Add( new StudentSk("Bára ", 1.2) );
            mujSeznam.Add( new StudentForeign("Leon ", true));

            foreach (Student s in mujSeznam)
            {
                s.GetCaption();
            }
                        
            Console.ReadLine();
        }
    }
}
