﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    class StudentSk:Student
    {
        private double studijniPrumer;

        public StudentSk(string slovenskeJmeno)
            : base(slovenskeJmeno)
        {
            studijniPrumer = 0;
        }

        public StudentSk(string slovenskeJmeno, double prumerZnamek)
            : base(slovenskeJmeno)
        {
            studijniPrumer = prumerZnamek;
        }

        public override double CountScholarship()
        {
            double stipko = 0;
            if (delkaStudia > 6)
            {
                stipko = (delkaStudia - 6) * 5000;
            }
            if (studijniPrumer > 1.5)
            {
                stipko += delkaStudia * 500;
            }
            return stipko;
        }

        public override void GetCaption()
        {
            base.GetCaption();
            Console.WriteLine("Sk");
            Console.WriteLine(studijniPrumer);
        }
    }
}
