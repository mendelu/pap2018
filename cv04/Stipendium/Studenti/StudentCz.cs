﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    class StudentCz :Student
    {
        private double dojezdovaVzdalenost;

        public StudentCz(string ceskeJmeno)
            : base(ceskeJmeno)
        {
            dojezdovaVzdalenost = 0;
        }

        public StudentCz(string ceskeJmeno, double dojezd)
            : base(ceskeJmeno)
        {
            dojezdovaVzdalenost = dojezd;
        }

        public override double CountScholarship()
        {
            //if (delkaStudia <= 6) { return 0; }
            //return ((delkaStudia - 6) * 5000);
            return (delkaStudia < 6) ? 0 : ((delkaStudia - 6) * 5000);
        }

        public override void GetCaption()
        {
            base.GetCaption();
            Console.WriteLine("Cz");
            Console.WriteLine(dojezdovaVzdalenost);            
        }
    }
}
