﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JednostrannySeznam
{
    public class Student : IEquatable<Student>, ImojeBozskeRozhrani
    {
        string jmeno;

        public Student(string jm)
        {
            jmeno = jm;
        }
        

        
        public bool Equals(Student other)
        {
            return (jmeno.Equals(other.jmeno));
        }

        public int GetScore()
        {
            return jmeno.Length;
        }

        public override string ToString()
        {
            return "Student - " + jmeno;
        }
    }
}
