﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JednostrannySeznam
{
    class Program
    {
        static void Main(string[] args)
        {
            Polozka prvni = new Polozka();
            Polozka magicStick;
            magicStick = prvni.PridejNaslednika(1);
            magicStick = magicStick.PridejNaslednika(2);
            prvni.PridejNaslednika(3);
            prvni.Vypis();            

            PolozkaOboustranna<Student> p1 = new PolozkaOboustranna<Student>();
            PolozkaOboustranna<Student> ms;
            Student pom = new Student("Adam");
            ms = p1.PridejNaslednika(pom);
            ms = ms.PridejNaslednika(new Student("Bara"));
            ms = ms.PridejNaslednika(new Student("Cyril"));           
            ms.VypisVse();

            PolozkaOboustranna<Student> nalezena = ms.NajdiData(new Student("Cyril"));
            if (nalezena != null) { Console.WriteLine("nalezeno"); }

            Console.WriteLine("celkove skore je {0}",ms.GetScore());

            Student pomData = ms[1];
            //ms[5] = new Student("David");
            Console.WriteLine( pomData);

            Console.ReadLine();
        }
    }
}
