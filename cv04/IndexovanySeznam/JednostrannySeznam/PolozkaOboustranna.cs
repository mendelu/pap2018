﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JednostrannySeznam
{
    public class PolozkaOboustranna<T> where T:class, IEquatable<T>, ImojeBozskeRozhrani
    {
        private T data;

        public T Data
        {
            get { return data; }
            set { data = value; }
        }

        private PolozkaOboustranna<T> next;
        private PolozkaOboustranna<T> prev;

        public PolozkaOboustranna<T> PridejNaslednika(T novadata)
        {
            PolozkaOboustranna<T> newItem = new PolozkaOboustranna<T>();
            newItem.data = novadata;
            newItem.next = this.next; //next
            newItem.prev = this;
            if (this.next != null)
            {
                this.next.prev = newItem;
            }
            this.next = newItem;
            return newItem;
        }

        public bool OdeberNaslednika()
        {
            bool ok = false;
            if (next == null) { return ok; }
            this.next = next.next;
            ok = true;
            return ok;
        }

        public void Vypis()
        {            
            Console.WriteLine(data);
            if (next != null)
            {
                next.Vypis();
            }
        }

        public void VypisVse()
        {
            PolozkaOboustranna<T> ukazovatko = this;
            while (ukazovatko.prev != null)
            {
                ukazovatko = ukazovatko.prev;
            }

            ukazovatko.Vypis();
        }

        public T this[byte idx]
        {
            get
            {
                PolozkaOboustranna<T> ukazovatko = this;
                while (ukazovatko.prev != null)
                {
                    ukazovatko = ukazovatko.prev;
                }

                int i = 0;
                do
                {
                    if (i == idx)
                    {
                        return ukazovatko.Data;
                    }
                    i++;
                    ukazovatko = ukazovatko.next;                    
                } while (ukazovatko != null);

                return null; //vyvolat vyjimku by bylo lepsi
            }            
        }

        public PolozkaOboustranna<T> NajdiData(T hledanaData)
        {
            PolozkaOboustranna<T> ukazovatko = this;
            while (ukazovatko.prev != null)
            {
                ukazovatko = ukazovatko.prev;
            }
            
            do
            {
                if ((ukazovatko.data != null)&&(ukazovatko.data.Equals(hledanaData)))
                {
                    return ukazovatko;
                }                
                ukazovatko = ukazovatko.next;
            } while (ukazovatko != null);
            return null;
        }

        public int GetScore()
        {
            PolozkaOboustranna<T> ukazovatko = this;
            while (ukazovatko.prev != null)
            {
                ukazovatko = ukazovatko.prev;
            }

            int suma = 0;
            ukazovatko = ukazovatko.next;
            while (ukazovatko != null)
            {
                suma += ukazovatko.data.GetScore();
                ukazovatko = ukazovatko.next;
            }
            return suma;
            
        }
    }
}
