﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Calc kalku = new Calc();
            kalku.Operand1 = 25;
            kalku.Operand2 = 7;
            int zb;
            float v = kalku.Vydel(out zb);
            string s = String.Format("Vysledek je {0} a zbytek je {1}", v, zb);
            Console.WriteLine(s);

            Console.ReadLine();
        }
    }
}
