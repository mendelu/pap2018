﻿using System;

namespace ConsoleApp3
{
    public class Calc
    {
        #region vlastnosti

        private float vysledek;

        public float Vysledek
        {
            get { return vysledek; }
        }

        public string VysledekAsStr
        {
            get
            {
                if (Single.IsInfinity(vysledek))
                {
                    return "nekonecno";
                }
                return vysledek.ToString("F3");                
            }
        }

        public int Operand1;
        
        private int operand2;

        public int Operand2
        {
            set
            {
                if (value < 0)
                {
                    operand2 = 0;
                }
                else
                {
                    operand2 = value;
                }
            }
            get
            {
                return operand2;
            }
        }

        #endregion

        public float Vydel(out int zbytek)
        {
            if (Single.IsNaN(Operand1) || Single.IsNaN(Operand2))
            {
                vysledek = Single.NaN;
                zbytek = 0;             
            }
            else
            {
                vysledek = (float)Operand1 / (float)operand2;
                zbytek = Operand1 % operand2;
            }

            return vysledek;
            }

    }
}
