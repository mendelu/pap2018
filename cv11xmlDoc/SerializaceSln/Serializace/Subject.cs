﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Runtime.Serialization;

namespace Kolekce
{
    public class Subject : ISerializable
    {
        private string name;
        public string Name { get { return name; } }

        private List<Lecture> lectures;

        public Subject(string nameOfSubject)
        {
            name = nameOfSubject;
            lectures = new List<Lecture>(10);
        }

        public void AddLecture(Lecture anotherone)
        {
            lectures.Add(anotherone);
        }

        public bool RemoveLecture(Lecture lectureToDele)
        {
            return lectures.Remove(lectureToDele);
        }

        public bool containsLecture(Lecture lectureToFind)
        {
            return lectures.Contains(lectureToFind); //vyzaduje IEquatable u Lecture
        }

        public override string ToString()
        {
            string pom = name + "\n";
            foreach (Lecture l in lectures)
            {
                pom += " " + l.ToString() + "\n";
            }
            return pom;
            
        }

        public IReadOnlyCollection<Lecture> GetLectures()
        {
            return lectures.AsReadOnly();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            string lecId;

            info.AddValue("subjectname", name);
            info.AddValue("numberOfLectures", lectures.Count);
            for (int i = 0; i < lectures.Count; i++)
            {
                lecId = "lecture_" + i.ToString();
                info.AddValue(lecId, lectures[i], typeof(Lecture));
            }
        }

        public Subject(SerializationInfo info, StreamingContext context)
        {
            lectures = new List<Kolekce.Lecture>();
            string lecId;
            Lecture pomLecture;
            int num = info.GetInt32("numberOfLectures");
            for (int i = 0; i < num; i++)
            {
                lecId = "lecture_" + i.ToString();
                pomLecture = (Lecture) info.GetValue(lecId, typeof(Lecture));
                lectures.Add(pomLecture);
            }
        }








        /*
public void safeToElement(ref XmlDocument doc, ref XmlElement subjectsElement)
{
   XmlElement subjectEl = doc.CreateElement("subject");
   subjectEl.SetAttribute("Jmeno",name);
   foreach (Lecture lec in lectures)
   {
       subjectEl.AppendChild(lec.SaveToElement(doc));
   }

   subjectsElement.AppendChild(subjectEl);
   return;
}
*/
    }
}
