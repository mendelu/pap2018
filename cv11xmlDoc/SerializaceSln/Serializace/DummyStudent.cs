﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KolekceProSerializaci
{    
    [Serializable]
    public class DummyStudent
    {
        private static int lastID = 0;

        [XmlElement("ajdycko")]
        public int ID { get; set; }

        [XmlAttribute("jmeeeeeeno")]
        public string Jmeno { get; set;}
        public string Prijmeni { get; set; }

        public DummyStudent(string jmeno, string prijmeni)
        {
            this.Jmeno = jmeno;
            this.Prijmeni = prijmeni;
            this.ID = lastID++;
        }
        
        public DummyStudent()
        { }
 

    }
}
