﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KolekceProSerializaci
{
    [Serializable]
    [XmlRoot("SubjectRoot")]
    public class DummySubject
    {        
        public string Jmeno { get; set; }

        [XmlArray("VyukoveJednotky"), XmlArrayItem(typeof(DummyLecture), ElementName = "Lecture")]
        public DummyLecture[] Lectures;

        public DummySubject(string name)
        {
            Jmeno = name;
            Lectures = new DummyLecture[100];
        }

        public void AddLectures(List<DummyLecture> dLectures)
        {
            Lectures = dLectures.ToArray();
        }

        public DummySubject()
        {
            Lectures = new DummyLecture[100];
        }
    }
}
