﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Kolekce
{
    
    [Serializable]
    public class Lecture : IEquatable<Lecture>, ISerializable
    {
        private DateTime start;
        public DateTime Start { get { return start; } }

        private string classRoom;
        public string ClassRoom { get { return classRoom; } }

        public Lecture(DateTime kdy, string kde)
        {
            start = kdy;
            classRoom = kde;
        }

        public Lecture()
        {
        }

        public override string ToString()
        {
            return String.Format("lecture {0} at {1}h", start.DayOfWeek.ToString(), start.Hour.ToString());
        }

        public bool Equals(Lecture other)
        {
            return ((start.DayOfWeek == other.Start.DayOfWeek) &
                (start.Hour.Equals(other.Start.Hour))&
                (classRoom.Equals(other.ClassRoom)));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("cRoom", classRoom, typeof(string));
            info.AddValue("start", start);
        }

        public Lecture(SerializationInfo info, StreamingContext context)
        {
            classRoom = (string) info.GetValue("cRoom", typeof(string));
            start = info.GetDateTime("start");
        }

    }
}
