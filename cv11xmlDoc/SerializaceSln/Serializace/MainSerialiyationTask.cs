﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using KolekceProSerializaci;
using Kolekce;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
namespace Serializace
{
    class MainSerialiyationTask
    {
        public void SerialiyeDummzStudent()
        {
            DummyStudent dms = new DummyStudent("Jan", "Novak");

            XmlSerializer mySerializer = new XmlSerializer(typeof(DummyStudent));
            StreamWriter sw = new StreamWriter("fileDummystudent.xml");
            mySerializer.Serialize(sw, dms);
        }

        public void SerializeDummySubject()
        {
            DummySubject ds = new DummySubject("PAP");
            List<DummyLecture> lects = new List<DummyLecture>();
            DateTime t1 = DateTime.Now;
            DateTime t2 = new DateTime(2018, 12, 1, 8, 0, 0);
            lects.Add(new DummyLecture(t1, "Q05"));
            lects.Add(new DummyLecture(t2, "Q06"));
            ds.AddLectures(lects);

            XmlSerializer mySerializer = new XmlSerializer(typeof(DummySubject));
            StreamWriter myWriter = new StreamWriter("fileDummySubject.xml");
            mySerializer.Serialize(myWriter, ds);
            myWriter.Close();
        }

        public void DeserializeDummySubject()
        {
            FileStream myFileStream = new FileStream("fileDummySubject.xml", FileMode.Open);
            XmlSerializer mySerializer = new XmlSerializer(typeof(DummySubject));
            DummySubject dm1 = (DummySubject)mySerializer.Deserialize(myFileStream);
            Console.WriteLine("{0} velikost {1}", dm1.Jmeno, dm1.Lectures.Length);
        }

        public void SerializeLecture()
        {
            //using System.Runtime.Serialization;
            //using System.Runtime.Serialization.Formatters.Binary;
            Lecture l1 = new Lecture(DateTime.Now, "Q05");

            FileStream myFileStream = new FileStream("fileLecture.xml", FileMode.Create);
            IFormatter myformater = new BinaryFormatter();

            myformater.Serialize(myFileStream, l1);
            myFileStream.Close();
        }

        public void DeserializeLecture()
        {
            FileStream myFileStream = new FileStream("fileLecture.xml", FileMode.Open);
            IFormatter myformater = new BinaryFormatter();
            Lecture l1 = (Lecture)myformater.Deserialize(myFileStream);
            myFileStream.Close();
        }

    }
}
