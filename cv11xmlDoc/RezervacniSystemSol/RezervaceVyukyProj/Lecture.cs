﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RezervaceVyukyProj
{
    public class Lecture : IEquatable<Lecture>
    {
        //den v týdnu + hodina začátku + místo konání
        private DateTime start;

        public DateTime Start { get { return start; } }

        private string classRoom;

        public string ClassRoom { get { return classRoom; } }

        public Lecture(DateTime start, string room)
        {
            this.start = start;
            this.classRoom = room;
        }

        public override string ToString()
        {
            string pom = start.DayOfWeek.ToString() + " " + start.Hour.ToString();
            return pom;
        }

        public bool Equals(Lecture other)
        {            
            return (
                (this.start.DayOfWeek.Equals(other.start.DayOfWeek)) &&
                (this.start.Hour.Equals(other.start.Hour)) &&
                (this.classRoom.Equals(other.classRoom))  );
        }

        public string GetID()
        {
            string pom = start.DayOfWeek.ToString() + this.start.Hour.ToString() + this.classRoom;
            return pom;                
        }

        public XmlElement SaveToElement(XmlDocument doc)
        {
            XmlElement lectureEl = doc.CreateElement("lecture");

            XmlElement startEl = doc.CreateElement("start");
            startEl.InnerText = this.start.ToString();
            lectureEl.AppendChild(startEl);

            XmlElement classroomEl = doc.CreateElement("classroom");
            classroomEl.InnerText = this.classRoom;
            lectureEl.AppendChild(classroomEl);

            return lectureEl;
        }

    }
}
