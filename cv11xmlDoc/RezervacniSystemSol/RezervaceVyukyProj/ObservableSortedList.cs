﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace RezervaceVyukyProj
{
    public class ObservableSortedList : SortedList<BaseInfo, Student>, INotifyCollectionChanged 
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public new bool Add(BaseInfo baseInfo, Student student)
        {
            if (this.ContainsKey(baseInfo)) { return false; }
            base.Add(baseInfo, student);
            if (CollectionChanged != null)
            {
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
            return true;
        }

        public new bool Remove(BaseInfo baseInfo)
        {
            bool odebrano = base.Remove(baseInfo);
            if (odebrano == false) { return false; }
            if (CollectionChanged != null)
            {
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
            return true;
        }

        public new Student this[BaseInfo key]
        {
            get
            {
                return base[key];
            }            
        }

        public List<Student> ToList()        
        {
            return this.Values.ToList<Student>();
        }

    }
}
