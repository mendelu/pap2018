﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace RezervaceVyukyProj
{
    public class ReservationSystem
    {
        //nahrazeno kvuli propojeni s GUI private SortedList<BaseInfo, Student> students = new SortedList<BaseInfo, Student>();
        public ObservableSortedList students = new ObservableSortedList();
        private Dictionary<string, Subject> subjects = new Dictionary<string, Subject>();
        private SortedDictionary<string, Reservation> reservations = new SortedDictionary<string, Reservation>();

        public bool CreateReservation(string jmeno, string prijmeni, string subjectName, Lecture lecture, out Reservation newReservation)
        {
            newReservation = null;

            Student who = GetStudent(jmeno, prijmeni);
            if (who == null) return false;

            Subject sub = GetSubject(subjectName);
            if (sub == null) return false;

            if (!sub.containsLecture(lecture)) return false;

            Reservation newone = new Reservation(jmeno, prijmeni, subjectName, lecture);
            if (reservations.ContainsKey(newone.GetID()))
            {                
                return false;
            }

            reservations.Add(newone.GetID(), newone);
            newReservation = newone;
            return true;
        }

        public bool AddSubject(string nameOfSubject)
        {
            if (subjects.ContainsKey(nameOfSubject)) return false;
            Subject newSubject = new Subject(nameOfSubject);
            subjects.Add(newSubject.Name, newSubject);
            return true;
        }

        public Subject GetSubject(string subjectName)
        {
            return subjects[subjectName];
        }

        public Lecture AddLectureForSubject(string subjectName, DateTime start, string classRoom)
        {
            if (subjects.ContainsKey(subjectName) == false)
            {
                Console.WriteLine("nelze pridat rozvrhovou jednotku do neexistujiciho predmetu");
                return null;
            }

            Lecture newLecture = new Lecture(start, classRoom);
            if (subjects[subjectName].containsLecture(newLecture))
            {
                Console.WriteLine("nelze pridat, rozvrhova jednotka jiz je k predmetu pridana");
                return null;
            }

            subjects[subjectName].AddLecture(newLecture);
            return newLecture;
        }

        public bool AddStudent(string jmeno, string prijmeni)
        {
            Student newS = new Student(jmeno, prijmeni);
            if (students.ContainsKey(newS.Info)) return false;
            students.Add(newS.Info, newS);
            return true;
        }
        
        public bool RemoveStudent(string prijmeni, string jmeno)
        {
            Student newS = new Student(jmeno, prijmeni);
            return students.Remove(newS.Info);
        }

        public Student GetStudent(string jmeno, string prijmeni)
        {
            Student newS = new Student(jmeno, prijmeni);
            if (!students.ContainsKey(newS.Info)) return null;
            return students[newS.Info];
        }

        public void PrintStudents()
        {
            foreach (KeyValuePair<BaseInfo, Student> kvp in students)
            {
                Console.WriteLine(kvp.Value.ToString());
            }
        }

        private XmlDocument SaveToXMLDocument()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement mainElement = doc.CreateElement("reservacnisystem");

            XmlElement subjectsEl = doc.CreateElement("subjects");

            foreach (Subject sub in subjects.Values)
            {
                sub.safeToElement(ref doc, ref subjectsEl);
            }

            mainElement.AppendChild(subjectsEl);
            doc.AppendChild(mainElement);
            return doc;
        }

        public string SaveToXmlString()
        {
            XmlDocument doc = this.SaveToXMLDocument();

            StringBuilder builder = new StringBuilder();
            using (StringWriter sWriter = new StringWriter(builder))
            using (XmlTextWriter xtWriter = new XmlTextWriter(sWriter))
            {
                xtWriter.Formatting = Formatting.Indented;
                doc.WriteContentTo(xtWriter);
            }

            return builder.ToString();
        }

        public string SaveToXmlFile(string pth)
        {
            XmlDocument doc = this.SaveToXMLDocument();

            StringBuilder builder = new StringBuilder();
            using (StringWriter sWriter = new StringWriter(builder))
            using (XmlTextWriter xtWriter = new XmlTextWriter(pth,Encoding.UTF8))
            {
                xtWriter.Formatting = Formatting.Indented;
                doc.WriteContentTo(xtWriter);
            }

            return builder.ToString();
        }

        public void LoadFromXmlString(string xmlFileAsString)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlFileAsString);

            XmlNodeList xnListSubjects = doc.GetElementsByTagName("subject");

            //xnListSubjects[0].Attributes....

            //lectures od prvniho subject:
            XmlNodeList xnListLectures = xnListSubjects[0].ChildNodes;
        }
    }
}
