﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RezervaceVyukyProj
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime pom = new DateTime(2018, 11, 2, 8, 0, 0);
            Lecture l1 = new Lecture(pom, "Q05");
            Console.WriteLine(l1);

            ReservationSystem rs = new ReservationSystem();
            rs.AddStudent("Jan", "Novak");
            rs.AddStudent("Jana", "Novakova");
            rs.GetStudent("Alan", "Alda");

            rs.AddSubject("PAP");
            rs.AddSubject("CPP");
            rs.AddSubject("VUI");

            Lecture pomLecture1 = rs.AddLectureForSubject("PAP", new DateTime(2018, 11, 12,8,0,0), "Q05");
            Lecture pomLecture2 = rs.AddLectureForSubject("PAP", new DateTime(2018, 11, 13,10,0,0), "Q06");

            Reservation newOne;
            rs.CreateReservation("Jan", "Novak", "PAP", pomLecture1, out newOne);
            if (!rs.CreateReservation("Jan", "Novak", "PAP", pomLecture1, out newOne))
            {
                Console.WriteLine("nelze pridat rezervaci");
            }

            Console.ReadLine();
        }
    }
}
