﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RezervaceVyukyProj;

namespace GUIproj
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ReservationSystem rs ;

        public MainWindow()
        {
            InitializeComponent();
            SetCalendar();
        }

        private void SetCalendar()
        {
            Calendar1.DisplayDateStart = new DateTime(2018, 11, 1);
            Calendar1.DisplayDateEnd = new DateTime(2018, 11, 30);
            Calendar1.DisplayMode = CalendarMode.Month;
            Calendar1.FirstDayOfWeek = DayOfWeek.Monday;
            Calendar1.BlackoutDates.Add(new CalendarDateRange(new DateTime(2018, 11, 3), new DateTime(2018, 11, 4)));
            Calendar1.BlackoutDates.Add(new CalendarDateRange(new DateTime(2018, 11, 10), new DateTime(2018, 11, 11)));

            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            rs = new ReservationSystem();
            MainGrid.DataContext = rs;
            ListBoxStudents.ItemsSource = rs.students;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            rs.AddStudent(TextBoxJmeno.Text, TextBoxPrijmeni.Text);
            //Student s = rs.GetStudent(TextBoxJmeno.Text, TextBoxPrijmeni.Text);
            //ListBoxStudents.Items.Add(s);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (rs.AddSubject(TextBoxSubject.Text) == false) { return; }
            Subject s = rs.GetSubject(TextBoxSubject.Text);
            ListBoxSubjects.Items.Add(s);
        }

        private void ButttonAddLecture_Click(object sender, RoutedEventArgs e)
        {
            if (ListBoxSubjects.SelectedItem == null)
            {
                MessageBox.Show("Vyberte subject!", "error", MessageBoxButton.OK);
                return;
            }

            if (Calendar1.SelectedDate == null)
            {
                MessageBox.Show("Vyberte den v tydnu!", "error", MessageBoxButton.OK);
                return;
            }

            DateTime when = Calendar1.SelectedDate.Value;
            double h = Convert.ToDouble(ComboboxHours.SelectedItem);
            when = when.AddHours(h);
            Lecture newOne = new Lecture(when, TextboxClassroom.Text);
            //(ListBoxSubjects.SelectedItem as Subject).AddLecture(newOne);

            Lecture pom = rs.AddLectureForSubject((ListBoxSubjects.SelectedItem as Subject).Name, when, TextboxClassroom.Text);
            //ListboxLectures.Items.Add(pom);
            ListBoxSubjects_SelectionChanged(this, null);

        }

        private void ListBoxSubjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListboxLectures.ItemsSource = (ListBoxSubjects.SelectedItem as Subject).GetLectures();

            /*
            ListboxLectures.Items.Clear();
            if (ListBoxSubjects.SelectedItem == null) { return; }
            Subject vybrany = (Subject)ListBoxSubjects.SelectedItem;
            foreach (Lecture lec in vybrany.GetLectures())
            {
                ListboxLectures.Items.Add(lec);
            }
            */
        }

        private void ComboboxHours_Initialized(object sender, EventArgs e)
        {
            for (int i = 7; i <= 18; i++) { ComboboxHours.Items.Add(i); }
        }

        private void ButtonAddReservation_Click(object sender, RoutedEventArgs e)
        {
            Student who = ListBoxStudents.SelectedItem as Student;
            Subject what = ListBoxSubjects.SelectedItem as Subject;
            Lecture which = ListboxLectures.SelectedItem as Lecture;

            Reservation newRes;
            if (rs.CreateReservation(who.Info.Jmeno, who.Info.Prijmeni, what.Name, which, out newRes))
            {
                ListboxReservations.Items.Add(newRes);
            }

        }

        private void ButtonExportXml_Click(object sender, RoutedEventArgs e)
        {
            TextBlockXML.Text = rs.SaveToXmlString();
            rs.SaveToXmlFile("reservacnisystem.xml");
        }
    }
}
