﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public static class SingletonNameChecker
    {
        private static readonly NameChecker instance = new NameChecker();

        //private SingletonNameChecker() { }

        public static NameChecker Instance
        {
            get
            {
                return instance;
            }
        }

    }
}
