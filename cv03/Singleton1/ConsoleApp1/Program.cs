﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            NameChecker n1 = NameChecker.getInstance();
            n1.AddName("a");
            n1.AddName("b");            
            NameChecker n2 = NameChecker.getInstance();
            n2.AddName("c");
            n1.Print();
            */

            NameChecker n1 = SingletonNameChecker.Instance;
            n1.AddName("a");
            n1.AddName("b");
            NameChecker n2 = SingletonNameChecker.Instance;
            n2.AddName("c");
            n1.Print();

            Console.ReadLine();
        }
    }
}
