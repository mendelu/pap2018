﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class NameChecker
    {
        private List<string> listOfNames;

        internal NameChecker()
        {
            listOfNames = new List<string>();
            
        }        

        public bool AddName(string name)
        {
            foreach (string s in listOfNames)
            {
                if (String.Equals(s, name))
                {
                    return false;
                }
            }
            listOfNames.Add(name);
            return true;
        }

        public void Print()
        {
            foreach (string s in listOfNames)
            {
                Console.WriteLine(s);
            }
        }

    }
}
