﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JednostrannySeznam
{
    public class PolozkaOboustranna 
    {
        private int data;

        public int Data
        {
            get { return data; }
            set { data = value; }
        }

        private PolozkaOboustranna next;
        private PolozkaOboustranna prev;

        public PolozkaOboustranna PridejNaslednika(int novadata)
        {
            PolozkaOboustranna newItem = new PolozkaOboustranna();
            newItem.data = novadata;
            newItem.next = this.next; //next
            newItem.prev = this;
            if (this.next != null)
            {
                this.next.prev = newItem;
            }
            this.next = newItem;
            return newItem;
        }

        public bool OdeberNaslednika()
        {
            bool ok = false;
            if (next == null) { return ok; }
            this.next = next.next;
            ok = true;
            return ok;
        }

        public void Vypis()
        {            
            Console.WriteLine(data);
            if (next != null)
            {
                next.Vypis();
            }
        }

        public void VypisVse()
        {
            PolozkaOboustranna ukazovatko = this;
            while (ukazovatko.prev != null)
            {
                ukazovatko = ukazovatko.prev;
            }

            ukazovatko.Vypis();
        }

    }
}
