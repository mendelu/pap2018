﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JednostrannySeznam
{
    public class Polozka
    {
        private int data;

        public int Data
        {
            get { return data; }
            set { data = value; }
        }

        private Polozka next;

        public Polozka PridejNaslednika(int novadata)
        {
            Polozka newItem = new Polozka();
            newItem.data = novadata;
            newItem.next = this.next; //next
            this.next = newItem;
            return newItem;
        }

        public bool OdeberNaslednika()
        {
            bool ok = false;
            if (next == null) { return ok; }
            this.next = next.next;
            ok = true;
            return ok;
        }

        public void Vypis()
        {
            Console.WriteLine(data);
            if (next != null)
            {
                next.Vypis();
            }            
        }

    }
}
