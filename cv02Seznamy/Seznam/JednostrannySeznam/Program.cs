﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JednostrannySeznam
{
    class Program
    {
        static void Main(string[] args)
        {
            Polozka prvni = new Polozka();
            Polozka magicStick;
            magicStick = prvni.PridejNaslednika(1);
            magicStick = magicStick.PridejNaslednika(2);
            prvni.PridejNaslednika(3);
            prvni.Vypis();

            

            PolozkaOboustranna p1 = new PolozkaOboustranna();
            PolozkaOboustranna ms;
            ms = p1.PridejNaslednika(10);
            ms = ms.PridejNaslednika(20);
            ms = ms.PridejNaslednika(30);

            ms.VypisVse();
            Console.ReadLine();
        }
    }
}
