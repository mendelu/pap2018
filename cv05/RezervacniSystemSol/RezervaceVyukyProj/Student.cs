﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RezervaceVyukyProj
{
    public struct BaseInfo : IComparable<BaseInfo>
    {
        private string jmeno;
        private string prijmeni;

        public string Jmeno { get { return jmeno; } }
        public string Prijmeni { get { return prijmeni; } }

        public BaseInfo(string jmeno, string prijmeni)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
        }

        public int CompareTo(BaseInfo other)
        {
            return (jmeno + prijmeni).CompareTo(other.jmeno + other.prijmeni);
        }
        
    }


    public class Student
    {
        private BaseInfo info;

        public BaseInfo Info
        {
            get { return info; }
        }

        public Student(string jmeno, string prijmeni)
        {
            info = new BaseInfo(jmeno, prijmeni);
        }

        public override string ToString()
        {
            return String.Format("Student jmeno: {0} {1}", info.Prijmeni, info.Jmeno);
        }

        public string ShowCaption
        {
            get
            {
                return String.Format("Student jmeno: {1} {0}", info.Prijmeni, info.Jmeno);
            }
            
        }
    }
}
