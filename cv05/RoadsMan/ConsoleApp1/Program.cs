﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            RoadManager rm = new RoadManager(10);
            rm.AddRoad(TypeOfRoad.dalnice, 255);
            rm.AddRoad(TypeOfRoad.nespecifikovano, 3);
            rm.AddRoad(TypeOfRoad.tridaI, 80);
            rm.AddRoad(TypeOfRoad.tridaII, 25);
            rm.AddRoad(TypeOfRoad.dalnice, 405);

            rm.WriteToConsole();
            Console.WriteLine(rm.GetIndexOfRoad(new Road(80, TypeOfRoad.tridaI)));

            List<Road> vysl = rm.GetRoadsAsList();
            //Road r1 = new Road(255, TypeOfRoad.dalnice);
            //Console.WriteLine(r1);

            Console.ReadLine();
        }
    }
}
