﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class RoadManager
    {
        private Road[] roads;

        public RoadManager(int maxNumOfRoads)
        {
            roads = new Road[maxNumOfRoads];
        }

        public bool AddRoad(TypeOfRoad tr, int length)
        {            
            for(int i = 0; i < roads.Length; i++)
            {
                if (roads[i] == null)
                {
                    roads[i] = new Road(length, tr);
                    return true;
                }
            }
            return false;
        }

        public void WriteToConsole()
        {
            foreach (Road r in roads)
            {
                Console.WriteLine(r);
            }
        }

        public int GetIndexOfRoad(Road rd)
        {
            return Array.IndexOf(roads, rd);
        }

        public List<Road> GetRoadsAsList()
        {
            List<Road> pom = roads.ToList<Road>();
            return pom;
            /*
            List<Road> pom = new List<Road>();
            
            foreach (Road r in roads)
            {
                pom.Add(r);
            }
            */
            return pom;
        }
    }
}
