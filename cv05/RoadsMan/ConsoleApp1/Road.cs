﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public enum TypeOfRoad { nespecifikovano, dalnice, tridaI, tridaII }

    public class Road:IEquatable<Road>
    {
        static int lastId = 0;
        private int id;
        private double km = 0;
        private TypeOfRoad roadType;

        public Road(int length, TypeOfRoad rt)
        {
            id = lastId++;
            km = length;
            roadType = rt;
        }

        public override string ToString()
        {
            string s = String.Format("silnice id {2} typu {0} delky {1}km", roadType, km, id);
            return s;
        }    

        public bool Equals(Road other)
        {
            return ((other.km == km) && (other.roadType == roadType));
        }
    }
}
