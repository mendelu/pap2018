﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ClassLibrary1
{
    public static class Knihovna
    {
        public static void AddMessage(string someText)
        {
            try
            {
                using (System.IO.StreamWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\log.txt", true))
                {
                    writer.WriteLine(someText);
                    writer.Flush();
                }
            }
            catch
            {
            }
        }
    }
}
